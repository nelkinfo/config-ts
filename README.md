# Config-TS #
Configuration files for TypeScript developments.

Add to your `tsconfig.json` file:
```json
"extends": "@nelkinfo/config-ts/tsconfig.json"
```
Add to your `.eslintrc.json` file:
```json
"extends": "./node_modules/@nelkinfo/config-ts/.eslintrc.json"
```
or
```json
"extends": [
  ...
  "./node_modules/@nelkinfo/config-ts/.eslintrc.json",
  ...
]
```
